## here some OVAs to play around web security in lamp environment

### [Kali](https://gitlab.com/mazenovi/WebSec-Kali)

Kali 1 offensive system with some bonus in 1337 folder on desktop

### [Proxy](https://gitlab.com/mazenovi/WebSec-Proxy)

ubuntu server with nginx reverse proxy on https://dum.my

### [Debian](https://gitlab.com/mazenovi/WebSec-Debian)

debian server with some vulnerable projects / CMS version

### [The Network](https://gitlab.com/mazenovi/WebSec-TheNetwork)

debian server with an ultravulnerable app

## installation

ova should be on the same NAT submask to  work properly. read [Make Your Own Sec Lab ](http://mazenovi.github.io/blog/2015/11/10/make-your-own-sec-lab/) for a quick oveview /  how to

## courses

you can read my courses on web security [here](http://doc.m4z3.me/?srch-term=hacking).
domain name are set to work out of the box with OVAs
